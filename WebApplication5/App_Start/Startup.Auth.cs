﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Owin;
using WebApplication5.Models;

namespace WebApplication5
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });
            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            app.UseTwitterAuthentication(
               consumerKey: "oeYELH4llRZDJEwd3bOuqTu9j",
               consumerSecret: "fPh3EBbGQfaCRlXfONaVQVUBMfD6EKTh88rVsGfPcLhSdtrlBg");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");
            var facebookOptions = new FacebookAuthenticationOptions()
            {
                AppId = "216981945455357",
                AppSecret = "b2757f2e2097b970b4d42fec6dbb2fed",
                BackchannelHttpHandler = new FacebookBackChannelHandler(),
                UserInformationEndpoint = "https://graph.facebook.com/v2.4/me?fields=id,email"
            };
            facebookOptions.Scope.Add("email");
            app.UseFacebookAuthentication(facebookOptions);
            //app.UseGoogleAuthentication();
        }
    }
}