﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;

namespace WebApplication5.Models
{
    public class Milestone
    {
        public int id { get; set; }
        public int iid { get; set; }
        public int project_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string state { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string due_date { get; set; }
        public string start_date { get; set; }
    }
    public class Assignee
    {
        public string name { get; set; }
        public string username { get; set; }
        public int id { get; set; }
        public string state { get; set; }
        public string avatar_url { get; set; }
        public string web_url { get; set; }
    }
    public class Author
    {
        public string name { get; set; }
        public string username { get; set; }
        public int id { get; set; }
        public string state { get; set; }
        public string avatar_url { get; set; }
        public string web_url { get; set; }
    }
    public class Issues
    {
        public int id { get; set; }
        public int iid { get; set; }
        public int project_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string state { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public List<object> labels { get; set; }
        public Milestone milestone { get; set; }
        public Assignee assignee { get; set; }
        public Author author { get; set; }
        public int user_notes_count { get; set; }
        public int upvotes { get; set; }
        public int downvotes { get; set; }
        public string due_date { get; set; }
        public bool confidential { get; set; }
        public object weight { get; set; }
        public string web_url { get; set; }
        public bool subscribed { get; set; }

        //-------timestates
        public int time_estimate { get; set; }
        public int total_time_spent { get; set; }
        public object human_time_estimate { get; set; }
        public object human_total_time_spent { get; set; }


    }

    public class Owner
    {
        public string name { get; set; }
        public string username { get; set; }
        public int id { get; set; }
        public string state { get; set; }
        public string avatar_url { get; set; }
        public string web_url { get; set; }
    }
    public class Namespace
    {
        public int id { get; set; }
        public string name { get; set; }
        public string path { get; set; }
        public string kind { get; set; }
        public string full_path { get; set; }
    }
    public class ProjectAccess
    {
        public int access_level { get; set; }
        public int notification_level { get; set; }
    }
    public class Permissions
    {
        public ProjectAccess project_access { get; set; }
        public object group_access { get; set; }
    }
    public class Projects
    {
        #region Properties
        public int id { get; set; }
        public string description { get; set; }
        public string default_branch { get; set; }
        public List<object> tag_list { get; set; }
        public bool @public { get; set; }
        public bool archived { get; set; }
        public int visibility_level { get; set; }
        public string ssh_url_to_repo { get; set; }
        public string http_url_to_repo { get; set; }
        public string web_url { get; set; }
        public Owner owner { get; set; }
        public string name { get; set; }
        public string name_with_namespace { get; set; }
        public string path { get; set; }
        public string path_with_namespace { get; set; }
        public bool container_registry_enabled { get; set; }
        public bool issues_enabled { get; set; }
        public bool merge_requests_enabled { get; set; }
        public bool wiki_enabled { get; set; }
        public bool builds_enabled { get; set; }
        public bool snippets_enabled { get; set; }
        public string created_at { get; set; }
        public string last_activity_at { get; set; }
        public bool shared_runners_enabled { get; set; }
        public bool lfs_enabled { get; set; }
        public int creator_id { get; set; }
        public Namespace @namespace { get; set; }
        public object avatar_url { get; set; }
        public int star_count { get; set; }
        public int forks_count { get; set; }
        public int open_issues_count { get; set; }
        public bool public_builds { get; set; }
        public List<object> shared_with_groups { get; set; }
        public bool only_allow_merge_if_build_succeeds { get; set; }
        public bool request_access_enabled { get; set; }
        public bool? only_allow_merge_if_all_discussions_are_resolved { get; set; }
        public int approvals_before_merge { get; set; }
        public Permissions permissions { get; set; }
        #endregion
        private static string accesstoken = "yU-zEnG3TbP3jRMRLhT_";
        public static List<Projects> GetProject()
        {
            string url = "https://gitlab.com/api/v3/projects/";
            var page = 1;
            List<Projects> list = null;
            var Projects = new List<Projects>();
            var errProjects = new List<Projects>();
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
                HttpResponseMessage response;

                response = client.GetAsync("?per_page=50&page=" + page++).Result;

                if (response.IsSuccessStatusCode)
                {
                    var rawResponseContent = response.Content.ReadAsStringAsync();
                    list = new JavaScriptSerializer().Deserialize<List<Projects>>(rawResponseContent.Result);
                    Projects = Projects.Concat(list).ToList();
                }
            }
            catch (Exception ex)
            {
               // GetIssuesService.WriteErrorLog("Error occurred: " + ex.Message);
            }

            return Projects;
        }

    }


    public class Users
    {
        public string name { get; set; }
        public string username { get; set; }
        public int id { get; set; }
        public string state { get; set; }
        public string avatar_url { get; set; }
        public string web_url { get; set; }
        public int access_level { get; set; }
        public object expires_at { get; set; }
    }

}