﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class ConnectDB
    {
        public static string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        public static SqlConnection Connect()
        {
            var con = new SqlConnection(ConString);
            con.Open();
            return con;
        }
    }
}