﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TweetSharp;

namespace WebApplication5.Controllers
{
    public class LogController : Controller
    {
        string _consumerKey = "9m9qUnEJUiuYnQQEt5fKYkcrP";
        string _consumerSecret = "gW34Cf7xfABNzuk36VztdWYD8KoYCpcXyRxUx7yrk0JMRd4Xop";
        //
        // GET: /Log/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LogOut()
        {
            Session["UserName"] = null;
            Session["accessToken"] = null;
            return Redirect("Index");
        }
        public ActionResult Home()
        {
            if (Session["accessToken"] != null)
            {
                OAuthAccessToken accessToken = (OAuthAccessToken)Session["accessToken"];
                var service = new TwitterService(_consumerKey, _consumerSecret);
                service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
                ViewData["Tweets"] = service.ListTweetsOnHomeTimeline(new ListTweetsOnHomeTimelineOptions() { Count = 10 });
                return View();
            }
            Response.Write("Login Failed");
            return Redirect("Index");
        }

        [HttpPost]
        public ActionResult Index(string User, string Password)
        {
            if (User == "admin" && Password == "m2n1shlko")
            {
                Session["UserName"] = User;
                //Session["Password"] = Password;
                return RedirectToAction("IssuesList", "AssignedIssue");
            }
            ViewBag.Login = "Login failed";
            return View();
        }
        public ActionResult twitterLogin()
        {
            TwitterService service = new TwitterService(_consumerKey, _consumerSecret);
            var url = Url.Action("TwitterCallack", "Log", null, "http");
            OAuthRequestToken requestToken = service.GetRequestToken(url);
            Uri uri = service.GetAuthorizationUri(requestToken);
            return new RedirectResult(uri.ToString(), false);
        }
        public ActionResult TwitterCallack(string oauth_token, string oauth_verifier)
        {
            var requestToken = new OAuthRequestToken { Token = oauth_token };
            OAuthAccessToken accessToken;
            TwitterService service = new TwitterService(_consumerKey, _consumerSecret);
            accessToken = service.GetAccessToken(requestToken, oauth_verifier);
            if (accessToken != null)
            {
                Session["accessToken"] = accessToken;
                return Redirect("Home");
            }
            return Redirect("Index");
        }
    }
}