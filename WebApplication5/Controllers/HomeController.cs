﻿using Auth0.ManagementApi;
using Jose;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TweetSharp;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Net;
using System.IO;
using SelectPdf;
using System.Net.Http;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Http.Headers;
using System.Data.SqlClient;
using WebApplication5.Models;
using System.Data;
namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {
        string _consumerKey = "9m9qUnEJUiuYnQQEt5fKYkcrP";
        string _consumerSecret = "gW34Cf7xfABNzuk36VztdWYD8KoYCpcXyRxUx7yrk0JMRd4Xop";
        //////string _accessToken = "2697947342-02xnFsiRHHTVOy1anKQq2FSxcSvjEFVzwH4ARaE";
        //////string _accessTokenSecret = "EBMRn8dHzSckHTqympYT8B5O5aoK4e5mq9KeIAdfvnsS9";

        public ActionResult Index()
        {
            HashSet<string> urls = new HashSet<string>() { "http://www.microsoft.com", "http://www.google.com", "http://www.codeproject.com" };
            StringBuilder contentToWrite = new StringBuilder();
            string aa;

            //foreach (var url in urls)
            //{
            WebRequest request = WebRequest.Create("http://localhost:46713/Log/Index");
            WebResponse response = request.GetResponse();

            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
                aa = streamReader.ReadToEnd();
            }
            //break;
            // }


            // return RedirectToAction("JwtExample");

            return RedirectToAction("Index", "Log");

            //Convert to PDF by select pdf
            //HtmlToPdf converter = new HtmlToPdf();

            //// create a new pdf document converting an url 
            //PdfDocument doc = converter.ConvertUrl("https://www.google.co.in/");
            //// save pdf document 
            //doc.Save(System.Web.HttpContext.Current.Response, false, "Sample.pdf");

            //// close pdf document 
            //doc.Close();


            return View();
        }

        public ActionResult Authorize()
        {
            // Step 1 - Retrieve an OAuth Request Token
            TwitterService service = new TwitterService(_consumerKey, _consumerSecret);

            var url = Url.Action("AuthorizeCallback", "Home", null, "http");
            // This is the registered callback URL
            OAuthRequestToken requestToken = service.GetRequestToken(url);

            // Step 2 - Redirect to the OAuth Authorization URL
            Uri uri = service.GetAuthorizationUri(requestToken);
            return new RedirectResult(uri.ToString(), false /*permanent*/);
        }

        // This URL is registered as the application's callback at http://dev.twitter.com
        public ActionResult AuthorizeCallback(string oauth_token, string oauth_verifier)
        {
            var requestToken = new OAuthRequestToken { Token = oauth_token };
            OAuthAccessToken accessToken;
            // Step 3 - Exchange the Request Token for an Access Token
            TwitterService service = new TwitterService(_consumerKey, _consumerSecret);
            if (Session["accessToken"] != null)
            {
                accessToken = (OAuthAccessToken)Session["accessToken"];
            }
            else
            {
                accessToken = service.GetAccessToken(requestToken, oauth_verifier);
                Session["accessToken"] = accessToken;
            }
            // Step 4 - User authenticates using the Access Token
            service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
            VerifyCredentialsOptions option = new VerifyCredentialsOptions() { IncludeEntities = true };

            TwitterUser user = service.VerifyCredentials(option);
            user.Name = "aa";
            service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
            //int i = 0;
            var tweets = service.ListTweetsOnHomeTimeline(new ListTweetsOnHomeTimelineOptions() { Count = 50 });
            var tUser = service.GetUserProfile(new GetUserProfileOptions());
            //Response.Write("<br><br>");
            //foreach (var tweet in tweets)
            //{
            //    Response.Write("<div class='alert-danger' style='width:800px;margin:auto;border:2px solid gray;border-radius:10px;'><b>" + ++i + "-   </b>" + tweet.User.ScreenName + "-   " + tweet.Text + "</div><br>");
            //}
            ViewData["Tweets"] = service.ListTweetsOnHomeTimeline(new ListTweetsOnHomeTimelineOptions() { Count = 50 });

            return View();
        }

        [HttpPost]
        public ActionResult AuthorizeCallback(string txtTweet)
        {
            if (Session["accessToken"] != null)
            {
                OAuthAccessToken accessToken = (OAuthAccessToken)Session["accessToken"];
                var service = new TwitterService(_consumerKey, _consumerSecret);
                service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
                service.SendTweet(new SendTweetOptions() { InReplyToStatusId = 10, Status = txtTweet.Trim() });
                ViewData["Tweets"] = service.ListTweetsOnHomeTimeline(new ListTweetsOnHomeTimelineOptions() { Count = 50 });
            }
            return View();
        }

        public ActionResult search()
        {
            return Redirect("AuthorizeCallback");
        }
        [HttpPost]
        public ActionResult search(string search)
        {
            if (Session["accessToken"] != null)
            {
                try
                {
                    OAuthAccessToken accessToken = (OAuthAccessToken)Session["accessToken"];
                    var service = new TwitterService(_consumerKey, _consumerSecret);
                    service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
                    var tweets = service.Search(new SearchOptions { Q = "#" + search, Count = 100 });
                    TempData["Search"] = tweets.Statuses;
                    TempData["SearchKey"] = search;
                }
                catch (Exception e)
                {
                    Response.Write(e.Message);
                }
                return Redirect("AuthorizeCallback");
            }
            return Redirect("Authorize");
        }

        public string JwtExample()
        {
            byte[] secretKey = Base64UrlDecode("lRf5haIZkT5speWWRbBOyPeQgpCJZcQ0HYuPDgbbFUPxaMoNXHo9o5TNIAMi-4jQ");
            DateTime issued = DateTime.Now;
            DateTime expire = DateTime.Now.AddHours(10);

            var payload = new Dictionary<string, object>()
            {
                {"aud", "2QFGb-BCbqgWun1N2D6FwxiNsnn2aG7e"},
                {"alg","HS256"},
                {"typ","JWT"}
            };
            var payload11 = new Dictionary<string, object>()
            {
                {"a","{'id':4754127,'iid':1163,'project_id':1656863,'title':'Cover Letter Required- Development of mHealth platform','description':'Please provide cover for the following job description:\r\n\r\nLooking for the development of a web-based digital health platform in the field of mHealth. The platform will allow users to interact with health advisors in 1-on-1 (text messages, voice or video). Users relieve individualized recommendations according to actively (questionnaire) and passively (pedometer of smartphones) gathered data. Over time the personal health advisor is substituted with digital algorithms to give individualized recommendations for personal health.\r\n\r\n- this will be a prototype of an ai-health-platform targeting one specific chronic disease at first \r\n\r\n- the platform should be accessible via web-browser, iOS and android app\r\n\r\n- the code and development of the ai recommendation system is planned in further development \r\n\r\nBudget- $10k-25k\r\n\r\nProject Link- http://www.guru.com/jobs/development-of-mhealth-platform/1342327'}"},
                {"a1","{'id':4754127,'iid':1163,'project_id':1656863,'title':'Cover Letter Required- Development of mHealth platform','description':'Please provide cover for the following job description:\r\n\r\nLooking for the development of a web-based digital health platform in the field of mHealth. The platform will allow users to interact with health advisors in 1-on-1 (text messages, voice or video). Users relieve individualized recommendations according to actively (questionnaire) and passively (pedometer of smartphones) gathered data. Over time the personal health advisor is substituted with digital algorithms to give individualized recommendations for personal health.\r\n\r\n- this will be a prototype of an ai-health-platform targeting one specific chronic disease at first \r\n\r\n- the platform should be accessible via web-browser, iOS and android app\r\n\r\n- the code and development of the ai recommendation system is planned in further development \r\n\r\nBudget- $10k-25k\r\n\r\nProject Link- http://www.guru.com/jobs/development-of-mhealth-platform/1342327'}"}
            };
            //string payload1 = "{'alg':'HS256','typ':'JWT'}.{'aud':'2QFGb-BCbqgWun1N2D6FwxiNsnn2aG7e','alg':'HS256','typ':'JWT'}";
            string payload1 = "[{'id':4754127,'iid':1163,'project_id':1656863,'title':'Cover Letter Required- Development of mHealth platform','description':'Please provide cover for the following job description:\r\n\r\nLooking for the development of a web-based digital health platform in the field of mHealth. The platform will allow users to interact with health advisors in 1-on-1 (text messages, voice or video). Users relieve individualized recommendations according to actively (questionnaire) and passively (pedometer of smartphones) gathered data. Over time the personal health advisor is substituted with digital algorithms to give individualized recommendations for personal health.\r\n\r\n- this will be a prototype of an ai-health-platform targeting one specific chronic disease at first \r\n\r\n- the platform should be accessible via web-browser, iOS and android app\r\n\r\n- the code and development of the ai recommendation system is planned in further development \r\n\r\nBudget- $10k-25k\r\n\r\nProject Link- http://www.guru.com/jobs/development-of-mhealth-platform/1342327'}]";
            // payload1 = payload1 + "," + payload1;
            string token = JWT.Encode(payload1, secretKey, JwsAlgorithm.HS256);
            var client = new ManagementApiClient(token, new Uri("https://vikasrai.auth0.com/api/v2/"));
            var handler = new JwtSecurityTokenHandler();
            var tokenS = handler.ReadToken(token) as JwtSecurityToken;
            return "";

        }

        static byte[] Base64UrlDecode(string arg)
        {
            string s = arg;
            s = s.Replace('-', '+'); // 62nd char of encoding
            s = s.Replace('_', '/'); // 63rd char of encoding
            switch (s.Length % 4) // Pad with trailing '='s
            {
                case 0: break; // No pad chars in this case
                case 2: s += "=="; break; // Two pad chars
                case 3: s += "="; break; // One pad char
                default:
                    throw new System.Exception(
             "Illegal base64url string!");
            }
            return Convert.FromBase64String(s); // Standard base64 decoder
        }

        static long ToUnixTime(DateTime dateTime)
        {
            return (int)(dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public string LoadImage()
        {
            using (WebClient webClient = new WebClient())
            {
                byte[] data = webClient.DownloadData("https://api.qrserver.com/v1/create-qr-code/?data={name:vikas,mob:123,age:12}{name:vikas,mob:123,age:12}&size=200x500");
                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var yourImage = Image.FromStream(mem))
                    {
                        // If you want it as Png
                        yourImage.Save(Server.MapPath("~/aaa.png"), ImageFormat.Png);

                        // If you want it as Jpeg
                        yourImage.Save(Server.MapPath("~/aaa.jpg"), ImageFormat.Jpeg);
                    }
                }
                //using (SqlConnection con = ConnectDB.Connect())
                //{
                //    SqlCommand cmd = new SqlCommand();
                //    cmd.Connection = con;
                //    cmd.CommandText = "insert into Img (name)values(@Img)";
                //    cmd.Parameters.AddWithValue("@Img", data);
                //    int i = cmd.ExecuteNonQuery();
                //}
                using (SqlConnection con = ConnectDB.Connect())
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "select name from Img";
                    byte[] i = (byte[])cmd.ExecuteScalar();
                }
            }
            return "success";
        }

        public string LoadImage1()
        {
            string url = "https://api.qrserver.com/v1/create-qr-code/?data={name:vikas,mob:123,age:12}&size=200x500";
            //string url = "https://api.qrserver.com/v1/read-qr-code/?fileurl=https://api.qrserver.com/v1/create-qr-code/?data={name:vikas,mob:123,age:12}&size=200x500";
            var client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("image/png"));
            HttpResponseMessage response = client.GetAsync("").Result;
            if (response.IsSuccessStatusCode)
            {
                var rawResponseContent = response.Content.ReadAsStreamAsync();
                byte[] imageBytes;
                using (BinaryReader br = new BinaryReader(rawResponseContent.Result))
                {
                    imageBytes = br.ReadBytes((Int32)rawResponseContent.Result.Length);

                }
                FileStream fs = new FileStream(Server.MapPath("~/aaab.png"), FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                try
                {
                    bw.Write(imageBytes);
                }
                finally
                {
                    fs.Close();
                    bw.Close();
                }
            }
            return "success";
        }
    }
}