﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using WebApplication5.Models;
using System.Data;
namespace WebApplication5.Controllers
{
    public class AssignedIssueController : Controller
    {
        static HttpClient client = new HttpClient();
        //
        // GET: /AssignedIssue/
        public ActionResult Index()
        {
            //GetIssues();
            ViewBag.ProjectList = GetProjects();
            return View();
        }
        public ActionResult IssuesList()
        {
            if (Session["UserName"] == null)
                return RedirectToAction("Index", "Log");

            ViewBag.ProjectList = GetProjects();
            return View();
        }
        public ActionResult Index1()
        {
            ViewBag.ProjectList = GetProjects();
            return View();
        }

        private readonly string accesstoken = "yU-zEnG3TbP3jRMRLhT_";
        //public List<Issues> GetIssues()
        //{
        //    var Issues = new List<Issues>();
        //    var errIssues = new List<Issues>();
        //    try
        //    {
        //        foreach (var item in Projects.GetProject())
        //        {
        //            using (HttpClient client = new HttpClient())
        //            {
        //                List<Issues> list = null;
        //                string url = "https://gitlab.com/api/v3/projects/" + item.id + "/issues";
        //                var page = 1;
        //                client.BaseAddress = new Uri(url);
        //                client.DefaultRequestHeaders.Accept.Add(
        //                new MediaTypeWithQualityHeaderValue("application/json"));
        //                client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
        //                HttpResponseMessage response;
        //                while (list == null || list.Count > 0)
        //                {
        //                    response = client.GetAsync("?per_page=50&page=" + page++).Result;

        //                    if (response.IsSuccessStatusCode)
        //                    {
        //                        var rawResponseContent = response.Content.ReadAsStringAsync();
        //                        list = new JavaScriptSerializer().Deserialize<List<Issues>>(rawResponseContent.Result);
        //                        for (int i = 0; i < list.Count; i++)
        //                        {
        //                            using (var Client = new HttpClient())
        //                            {
        //                                try
        //                                {
        //                                    var TimeStates = new Issues();
        //                                    var Url = "https://gitlab.com/api/v3/projects/1656863/issues/" + list[i].id + "/time_stats";
        //                                    Client.BaseAddress = new Uri(Url);
        //                                    Client.DefaultRequestHeaders.Accept.Add(
        //                                    new MediaTypeWithQualityHeaderValue("application/json"));
        //                                    Client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
        //                                    response = Client.GetAsync("").Result;
        //                                    if (response.IsSuccessStatusCode)
        //                                    {
        //                                        var RawResponseContent = response.Content.ReadAsStringAsync();
        //                                        TimeStates = new JavaScriptSerializer().Deserialize<Issues>(RawResponseContent.Result);
        //                                        list[i].time_estimate = TimeStates.time_estimate;
        //                                        list[i].total_time_spent = TimeStates.total_time_spent;
        //                                        list[i].human_time_estimate = TimeStates.human_time_estimate;
        //                                        list[i].human_total_time_spent = TimeStates.human_total_time_spent;
        //                                    }

        //                                }
        //                                catch (System.AggregateException ex)
        //                                {
        //                                    errIssues.Add(list[i]);
        //                                }
        //                                catch (Exception ex)
        //                                {
        //                                   // GetIssuesService.WriteErrorLog("Error occurred: " + ex.Message);
        //                                }
        //                            }
        //                        }
        //                        Issues = Issues.Concat(list).ToList();
        //                    }
        //                  //  GetIssuesService.WriteErrorLog("count: " + Issues.Count);
        //                    //break;
        //                }
        //                for (int i = 0; i < errIssues.Count; i++)
        //                {
        //                    using (var Client = new HttpClient())
        //                    {
        //                        try
        //                        {
        //                            var TimeStates = new Issues();
        //                            var Url = "https://gitlab.com/api/v3/projects/1656863/issues/" + errIssues[i].id + "/time_stats";
        //                            Client.BaseAddress = new Uri(Url);
        //                            Client.DefaultRequestHeaders.Accept.Add(
        //                            new MediaTypeWithQualityHeaderValue("application/json"));
        //                            Client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
        //                            response = Client.GetAsync("").Result;
        //                            if (response.IsSuccessStatusCode)
        //                            {
        //                                var RawResponseContent = response.Content.ReadAsStringAsync();
        //                                TimeStates = new JavaScriptSerializer().Deserialize<Issues>(RawResponseContent.Result);
        //                                errIssues[i].time_estimate = TimeStates.time_estimate;
        //                                errIssues[i].total_time_spent = TimeStates.total_time_spent;
        //                                errIssues[i].human_time_estimate = TimeStates.human_time_estimate;
        //                                errIssues[i].human_total_time_spent = TimeStates.human_total_time_spent;
        //                            }

        //                        }
        //                        catch (Exception ex)
        //                        {
        //                           // GetIssuesService.WriteErrorLog("Error occurred: " + ex.Message);
        //                        }
        //                    }
        //                }
        //                Issues = Issues.Concat(errIssues).ToList();
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //       // GetIssuesService.WriteErrorLog("Error occurred: " + ex.Message);
        //    }

        //    return Issues;
        //}
        public List<ListItem> GetProjects()
        {
            string url = "https://gitlab.com/api/v3/projects/";
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
            HttpResponseMessage response = client.GetAsync("").Result;
            List<Projects> Projects = new List<Projects>();
            if (response.IsSuccessStatusCode)
            {
                var rawResponseContent = response.Content.ReadAsStringAsync();
                Projects = new JavaScriptSerializer().Deserialize<List<Projects>>(rawResponseContent.Result);
            }
            var ProjectList = new List<ListItem>();
            ProjectList = Projects.Select(x => new ListItem() { Text = x.name, Value = x.id.ToString() }).ToList();
            //    ProjectList.Add(new ListItem() { Text = "select Project", Value = "-1",Selected=true });
            return ProjectList;
        }

        [HttpPost]
        public JsonResult GetUsers(string ProjectId)
        {
            string url = "https://gitlab.com/api/v3/projects/" + ProjectId + "/members";
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
            HttpResponseMessage response = client.GetAsync("?per_page=100").Result;
            List<Users> Users = new List<Users>();
            if (response.IsSuccessStatusCode)
            {
                var rawResponseContent = response.Content.ReadAsStringAsync();
                Users = new JavaScriptSerializer().Deserialize<List<Users>>(rawResponseContent.Result);
            }
            var UsersList = new List<ListItem>();
            UsersList = Users.Select(x => new ListItem() { Text = x.username, Value = x.id.ToString() }).OrderBy(x => x.Text).ToList();
            return Json(UsersList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIssuesFromAPI(string ProjectId, string UserId)
        {
            string url = "https://gitlab.com/api/v3/projects/" + ProjectId + "/issues";
            var page = 1;
            List<Issues> list = null;
            var Issues = new List<Issues>();

            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
            HttpResponseMessage response;

            while (list == null || list.Count > 0)
            {
                response = client.GetAsync("?per_page=100&assignee_id=" + UserId + "&page=" + page++).Result;

                if (response.IsSuccessStatusCode)
                {
                    var rawResponseContent = response.Content.ReadAsStringAsync();
                    list = new JavaScriptSerializer().Deserialize<List<Issues>>(rawResponseContent.Result);
                    Issues = Issues.Concat(list).ToList();
                }
            }
            var TimeStates = new Issues();
            for (int i = 0; i < Issues.Count; i++)
            {
                url = "https://gitlab.com/api/v3/projects/" + ProjectId + "/issues/" + Issues[i].id + "/time_stats";
                client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
                response = client.GetAsync("").Result;
                List<Users> Users = new List<Users>();
                if (response.IsSuccessStatusCode)
                {
                    var rawResponseContent = response.Content.ReadAsStringAsync();
                    TimeStates = new JavaScriptSerializer().Deserialize<Issues>(rawResponseContent.Result);
                    Issues[i].time_estimate = TimeStates.time_estimate;
                    Issues[i].total_time_spent = TimeStates.total_time_spent;
                    Issues[i].human_time_estimate = TimeStates.human_time_estimate;
                    Issues[i].human_total_time_spent = TimeStates.human_total_time_spent;

                }
            }
            int ii = 1;
            List<iss> a = Issues.Select(x => new iss() { SrNo = ii++, id = x.id, title = x.title, assignee = x.assignee.name, timeEst = x.time_estimate, timeSpent = x.total_time_spent, due_date = x.due_date, Status = x.state }).ToList();
            return Json(a, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIssues(string ProjectId, string UserId)
        {
            List<iss> Issues = new List<iss>();
            using (SqlConnection con = ConnectDB.Connect())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcIssues";
                cmd.Parameters.AddWithValue("@project_id", ProjectId);
                cmd.Parameters.AddWithValue("@assigneeId", UserId);
                cmd.Parameters.AddWithValue("@SType", "GetIssues");
                DataTable dt = new DataTable();
                (new SqlDataAdapter(cmd)).Fill(dt);
                int i = 1;
                Issues = dt.AsEnumerable().Select(x => new iss() { SrNo = i++, id = x.Field<int>("Id"), title = x.Field<string>("title"), assignee = x.Field<string>("username"), timeEst = x.Field<int>("time_estimate"), timeSpent = x.Field<int>("total_time_spent"), due_date = x.Field<string>("due_date") == null ? "" : x.Field<string>("due_date"), Status = x.Field<string>("state"), created_at = x.Field<string>("created_at").Substring(0, 10), human_time_estimate = x.Field<string>("human_time_estimate"), human_total_time_spent = x.Field<string>("human_total_time_spent") }).ToList();
            }
            return Json(Issues, JsonRequestBehavior.AllowGet);
        }

        public string InsertIssues(List<Issues> Issues)
        {
            var msg = "aaaaaaaa";
            using (SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=GitLabApi;Integrated Security=True;"))
            {
                con.Open();
                SqlTransaction trans = con.BeginTransaction();
                try
                {

                    foreach (var item in Issues)
                    {
                        if (item.milestone != null)
                        {
                            SqlCommand cmdMilestone = new SqlCommand();
                            cmdMilestone.Connection = con;
                            cmdMilestone.Transaction = trans;
                            cmdMilestone.CommandType = CommandType.StoredProcedure;
                            cmdMilestone.CommandText = "ProcMilestone";
                            cmdMilestone.Parameters.AddWithValue("@id", item.milestone.id);
                            cmdMilestone.Parameters.AddWithValue("@iId", item.milestone.iid);
                            cmdMilestone.Parameters.AddWithValue("@title", item.milestone.title);
                            cmdMilestone.Parameters.AddWithValue("@description", item.milestone.description);
                            cmdMilestone.Parameters.AddWithValue("@state", item.milestone.state);
                            cmdMilestone.Parameters.AddWithValue("@created_at", item.milestone.created_at);
                            cmdMilestone.Parameters.AddWithValue("@updated_at", item.milestone.updated_at);
                            cmdMilestone.Parameters.AddWithValue("@due_date", item.milestone.due_date);
                            cmdMilestone.Parameters.AddWithValue("@start_date", item.milestone.start_date);
                            cmdMilestone.Parameters.AddWithValue("@SType", "Insert");
                            cmdMilestone.ExecuteNonQuery();
                        }
                        if (item.assignee != null)
                        {
                            SqlCommand cmdAssignee = new SqlCommand();
                            cmdAssignee.Connection = con;
                            cmdAssignee.Transaction = trans;
                            cmdAssignee.CommandType = CommandType.StoredProcedure;
                            cmdAssignee.CommandText = "ProcAssignee";
                            cmdAssignee.Parameters.AddWithValue("@name", item.assignee.name);
                            cmdAssignee.Parameters.AddWithValue("@username", item.assignee.username);
                            cmdAssignee.Parameters.AddWithValue("@id", item.assignee.id);
                            cmdAssignee.Parameters.AddWithValue("@state", item.assignee.state);
                            cmdAssignee.Parameters.AddWithValue("@avatar_url", item.assignee.avatar_url);
                            cmdAssignee.Parameters.AddWithValue("@web_url", item.assignee.web_url);
                            cmdAssignee.Parameters.AddWithValue("@SType", "Insert");
                            cmdAssignee.ExecuteNonQuery();
                        }
                        if (item.author != null)
                        {
                            SqlCommand cmdAuthor = new SqlCommand();
                            cmdAuthor.Connection = con;
                            cmdAuthor.Transaction = trans;
                            cmdAuthor.CommandType = CommandType.StoredProcedure;
                            cmdAuthor.CommandText = "ProcAuthor";
                            cmdAuthor.Parameters.AddWithValue("@name", item.author.name);
                            cmdAuthor.Parameters.AddWithValue("@username", item.author.username);
                            cmdAuthor.Parameters.AddWithValue("@id", item.author.id);
                            cmdAuthor.Parameters.AddWithValue("@state", item.author.state);
                            cmdAuthor.Parameters.AddWithValue("@avatar_url", item.author.avatar_url);
                            cmdAuthor.Parameters.AddWithValue("@web_url", item.author.web_url);
                            cmdAuthor.Parameters.AddWithValue("@SType", "Insert");
                            cmdAuthor.ExecuteNonQuery();
                        }
                        if (item != null)
                        {
                            SqlCommand cmdMilestone = new SqlCommand();
                            cmdMilestone.Connection = con;
                            cmdMilestone.Transaction = trans;
                            cmdMilestone.CommandType = CommandType.StoredProcedure;
                            cmdMilestone.CommandText = "ProcIssues";
                            cmdMilestone.Parameters.AddWithValue("@id", item.id);
                            cmdMilestone.Parameters.AddWithValue("@iid", item.iid);
                            cmdMilestone.Parameters.AddWithValue("@project_id", item.project_id);
                            cmdMilestone.Parameters.AddWithValue("@title", item.title);
                            cmdMilestone.Parameters.AddWithValue("@description", item.description);
                            cmdMilestone.Parameters.AddWithValue("@state", item.state);
                            cmdMilestone.Parameters.AddWithValue("@created_at", item.created_at);
                            cmdMilestone.Parameters.AddWithValue("@updated_at", item.updated_at);
                            cmdMilestone.Parameters.AddWithValue("@milestoneId", (item.milestone != null) ? item.milestone.id : 0);
                            cmdMilestone.Parameters.AddWithValue("@assigneeId", (item.assignee != null) ? item.assignee.id : 0);
                            cmdMilestone.Parameters.AddWithValue("@authorId", (item.author != null) ? item.author.id : 0);
                            cmdMilestone.Parameters.AddWithValue("@user_notes_count", item.user_notes_count);
                            cmdMilestone.Parameters.AddWithValue("@upvotes", item.upvotes);
                            cmdMilestone.Parameters.AddWithValue("@downvotes", item.downvotes);
                            cmdMilestone.Parameters.AddWithValue("@due_date", item.due_date);
                            cmdMilestone.Parameters.AddWithValue("@confidential", item.confidential);
                            cmdMilestone.Parameters.AddWithValue("@weight", item.weight);
                            cmdMilestone.Parameters.AddWithValue("@web_url", item.web_url);
                            cmdMilestone.Parameters.AddWithValue("@subscribed", item.subscribed);
                            cmdMilestone.Parameters.AddWithValue("@time_estimate", item.time_estimate);
                            cmdMilestone.Parameters.AddWithValue("@total_time_spent", item.total_time_spent);
                            cmdMilestone.Parameters.AddWithValue("@human_time_estimate", item.human_time_estimate);
                            cmdMilestone.Parameters.AddWithValue("@human_total_time_spent", item.human_total_time_spent);
                            cmdMilestone.Parameters.AddWithValue("@SType", "Insert");
                            msg = item.id.ToString();
                            cmdMilestone.ExecuteNonQuery();
                        }
                    }
                    trans.Commit();
                    msg = "success";
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    msg = msg + "Something went wrong:- " + ex.Message;
                }
            }


            return msg;

        }
    }
    public class iss
    {
        public int SrNo { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string assignee { get; set; }
        public int timeSpent { get; set; }
        public int timeEst { get; set; }
        public object human_time_estimate { get; set; }
        public object human_total_time_spent { get; set; }
        public string due_date { get; set; }
        public string created_at { get; set; }
        public string Status { get; set; }

    }



}