﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class GitLabApiController : Controller
    {
        static readonly string accessToken = "2UkUXvuZ3saFcxPm9Et_";
        // static readonly string url = "https://gitlab.com/api/v3/issues";
        //static readonly string url = "https://gitlab.com/api/v3/projects/owned";
        // static readonly string url = "https://gitlab.com/api/v3/projects/1656863/issues";
        //static readonly string url = "https://gitlab.com/api/v3/projects/1656863/issues/4756822";
        static readonly string url = "https://gitlab.com/api/v3/projects/";
        static HttpClient client = new HttpClient();
        //
        // GET: /GitLabApi/
        public ActionResult Issues()
        {
            client = new HttpClient();
            string json = "[{'id':4754127,'iid':1163,'project_id':1656863,'title':'Cover Letter Required- Development of mHealth platform','description':'Please provide cover for the following job description:\r\n\r\nLooking for the development of a web-based digital health platform in the field of mHealth. The platform will allow users to interact with health advisors in 1-on-1 (text messages, voice or video). Users relieve individualized recommendations according to actively (questionnaire) and passively (pedometer of smartphones) gathered data. Over time the personal health advisor is substituted with digital algorithms to give individualized recommendations for personal health.\r\n\r\n- this will be a prototype of an ai-health-platform targeting one specific chronic disease at first \r\n\r\n- the platform should be accessible via web-browser, iOS and android app\r\n\r\n- the code and development of the ai recommendation system is planned in further development \r\n\r\nBudget- $10k-25k\r\n\r\nProject Link- http://www.guru.com/jobs/development-of-mhealth-platform/1342327'}]";
            JavaScriptSerializer js = new JavaScriptSerializer();
            Issues[] persons = js.Deserialize<Issues[]>(json);
            string urlParameters = "?state=opened";
            urlParameters = "";
            client.BaseAddress = new Uri(url);
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", "yU-zEnG3TbP3jRMRLhT_");
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var rawResponseContent = response.Content.ReadAsStringAsync();
                string aa = rawResponseContent.Result.Replace("'", string.Empty);
                List<Projects> Projects = new JavaScriptSerializer().Deserialize<List<Projects>>(aa);
                ViewBag.Projects = Projects;
                //for (int i = 0; i < issues.Length; i++)
                //{
                //    string urlIss = "https://gitlab.com/api/v3/projects/" + issues[i].id + "/issues";
                //    client = new HttpClient();
                //    client.BaseAddress = new Uri(urlIss);
                //    client.DefaultRequestHeaders.Accept.Add(
                //        new MediaTypeWithQualityHeaderValue("application/json"));
                //    client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", "yU-zEnG3TbP3jRMRLhT_");

                //    HttpResponseMessage responseIss = client.GetAsync("?per_page=10000&max=10000").Result;
                //    var a = responseIss.Content.ReadAsStringAsync();
                //    RootObject[] RootObject = new JavaScriptSerializer().Deserialize<RootObject[]>(a.Result);
                //}
            }
            else
            {
                ViewBag.Issues = "Error. Response code: " + response.StatusCode.ToString();
            }
            return View();
        }

        public ActionResult assignees()
        {
            return View();
        }
        [HttpPost]
        public ActionResult assignees(string Assignee, List<Issues> Issues)
        {
            string urlIss = "https://gitlab.com/api/v3/projects/" + "1656863" + "/issues";
            var list = Issues;
            var page = 1;
            while (list == null || list.Count > 0)
            {
                client = new HttpClient();
                client.BaseAddress = new Uri(urlIss);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", "yU-zEnG3TbP3jRMRLhT_");
                HttpResponseMessage responseIss = client.GetAsync("?per_page=100&assignee_id=" + Assignee + "&page=" + page++).Result;
                var a = responseIss.Content.ReadAsStringAsync();
                list = new JavaScriptSerializer().Deserialize<List<Issues>>(a.Result);
                if (Issues != null)
                    Issues = Issues.Concat(list).ToList();
                else
                    Issues = list;
            }
            return View(Issues);
        }
        [HttpPost]
        public ActionResult Status(string status, List<Issues> Issues)
        {
            string urlIss = "https://gitlab.com/api/v3/projects/" + "1656863" + "/issues";
            var page = 1;
            var list = Issues;
            while (list == null || list.Count > 0)
            {
                client = new HttpClient();
                client.BaseAddress = new Uri(urlIss);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", "yU-zEnG3TbP3jRMRLhT_");
                HttpResponseMessage responseIss = client.GetAsync("?per_page=100&state=" + status + "&page=" + page++).Result;
                var a = responseIss.Content.ReadAsStringAsync();
                list = new JavaScriptSerializer().Deserialize<List<Issues>>(a.Result);
                if (Issues != null)
                    Issues = Issues.Concat(list).ToList();
                else
                    Issues = list;
            }
            TempData["Issues"] = Issues;
            return Redirect("assignees");
        }
    }
}